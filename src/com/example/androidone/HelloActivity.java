package com.example.androidone;

import java.util.Locale;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class HelloActivity extends Activity {
	
	public static String TAG = "HelloActivity";
	
	//private TextView login_hello_lblHello;
	private CheckBox hello_chkSexoDuro;
	private CheckBox hello_chkSexoNormal;
	private RadioGroup hello_rgrEstadoCivil;
	private RadioButton hello_radLibre;
	private RadioButton hello_radPreso;
	private RadioButton hello_radEnquilombado;
	private SeekBar hello_sbrEdad;
	private TextView hello_lblEdad;
	private Spinner hello_spnEstado;
	private RatingBar hello_rbrInteres;
	private Button hello_btnBack;
	private Button hello_btnSave;
	private TextView hello_lblHello;
	
	private SharedPreferences prefs;
	private String username;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_hello);
		
		// Cargo la referencia al archivo de preferencias
        prefs = getSharedPreferences(PrefConsts._FILENAME, MODE_PRIVATE);
        
        // Obtener parámetros
        username = getIntent().getStringExtra(IntentKeys.LOGIN_USERNAME);
		
		bindAndAssign();
		init();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		init();
	}

	private void bindAndAssign() {
		// Binding
		hello_chkSexoDuro = (CheckBox)findViewById(R.id.hello_chkSexoDuro);
		hello_chkSexoNormal = (CheckBox)findViewById(R.id.hello_chkSexoNormal);
		hello_rgrEstadoCivil = (RadioGroup)findViewById(R.id.hello_rgrEstadoCivil);
		hello_radLibre = (RadioButton)findViewById(R.id.hello_radLibre);
		hello_radPreso = (RadioButton)findViewById(R.id.hello_radPreso);
		hello_radEnquilombado = (RadioButton)findViewById(R.id.hello_radEnquilombado);
		hello_sbrEdad = (SeekBar)findViewById(R.id.hello_sbrEdad);
		hello_lblEdad = (TextView)findViewById(R.id.hello_lblEdad);
		hello_spnEstado = (Spinner)findViewById(R.id.hello_spnEstado);
		hello_rbrInteres = (RatingBar)findViewById(R.id.hello_rbrInteres);
		hello_btnBack = (Button)findViewById(R.id.hello_btnBack);
		hello_btnSave = (Button)findViewById(R.id.hello_btnSave);
		hello_lblHello = (TextView)findViewById(R.id.hello_lblHello);
		
		// Assign Events
		hello_btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();				
			}
		});
		
		hello_btnSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String msg = validForm(); 
				if (msg.equals("")) {
					Editor editor = prefs.edit();
					editor.putBoolean(PrefConsts.SEXODURO_FIELD, hello_chkSexoDuro.isChecked());
					editor.putBoolean(PrefConsts.SEXONORMAL_FIELD, hello_chkSexoNormal.isChecked());
					editor.putInt(PrefConsts.ESTADOCIVIL_FIELD, hello_rgrEstadoCivil.indexOfChild(
							findViewById(hello_rgrEstadoCivil.getCheckedRadioButtonId())));
					editor.putInt(PrefConsts.EDAD_FIELD, hello_sbrEdad.getProgress());
					editor.putInt(PrefConsts.ESTADO_FIELD, hello_spnEstado.getSelectedItemPosition());
					editor.putFloat(PrefConsts.INTERES_FIELD, hello_rbrInteres.getRating());
					editor.commit();
					Toast toast = Toast.makeText(HelloActivity.this, "Encuesta guardada", Toast.LENGTH_SHORT);
					toast.show();
					finish();
				} else {
					Toast toast = Toast.makeText(HelloActivity.this, msg, Toast.LENGTH_SHORT);
					toast.show();
				}
			}
		});
		
		hello_sbrEdad.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {		
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				hello_lblEdad.setText(String.valueOf(progress));
				if (progress < 18) {
					hello_lblEdad.setTextColor(getResources().getColor(R.color.texto_error));
				} else {
					hello_lblEdad.setTextColor(getResources().getColor(R.color.texto_normal));
				}
			}
		});
		
	}
	
	protected String validForm() {
		String retorno = "";
		
		int edad = hello_sbrEdad.getProgress();
		if (edad < 18) {
			retorno = "Debe ser mayor de 18!";
		}
		
		return retorno;
	}

	private void init() {
		hello_lblHello.setText(username.toUpperCase());
		
		hello_chkSexoDuro.setChecked(prefs.getBoolean(PrefConsts.SEXODURO_FIELD, PrefConsts.SEXODURO_DEFAULT));
		hello_chkSexoNormal.setChecked(prefs.getBoolean(PrefConsts.SEXONORMAL_FIELD, PrefConsts.SEXONORMAL_DEFAULT));
		((RadioButton)hello_rgrEstadoCivil.getChildAt(
				prefs.getInt(
						PrefConsts.ESTADOCIVIL_FIELD, PrefConsts.ESTADOCIVIL_DEFAULT))).setChecked(true);
		hello_sbrEdad.setProgress(prefs.getInt(PrefConsts.EDAD_FIELD, PrefConsts.EDAD_DEFAULT));
		hello_spnEstado.setSelection(prefs.getInt(PrefConsts.ESTADO_FIELD, PrefConsts.ESTADO_DEFAULT));
		hello_rbrInteres.setRating(prefs.getFloat(PrefConsts.INTERES_FIELD, PrefConsts.INTERES_DEFAULT));
	}
}
