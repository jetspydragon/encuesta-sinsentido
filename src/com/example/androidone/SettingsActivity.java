package com.example.androidone;

import java.util.ArrayList;

import com.example.androidone.adapter.SettingsAdapter;
import com.example.androidone.objects.MenuItem;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

public class SettingsActivity extends Activity {

	private ListView lv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>();
		menuItems.add(new MenuItem(MenuItem.MENUTYPE_ITEM, "Prueba 1", false));
		menuItems.add(new MenuItem(MenuItem.MENUTYPE_ITEM, "Prueba 2", true));
		menuItems.add(new MenuItem(MenuItem.MENUTYPE_ITEM, "Prueba 3", true));
		menuItems.add(new MenuItem(MenuItem.MENUTYPE_ITEM, "Prueba 4", false));
		menuItems.add(new MenuItem(MenuItem.MENUTYPE_ITEM, "Prueba 5", true));
		
		SettingsAdapter sa = new SettingsAdapter(menuItems, SettingsActivity.this);
		lv = (ListView) findViewById(R.id.settings_list);
		lv.setAdapter(sa);
	}
	
}
