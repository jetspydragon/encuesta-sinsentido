package com.example.androidone;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity {

	public static String TAG = "MainActivity";
	
	private static String PASSWORD = "1234";
	
	private EditText login_txtUsuario;
	private EditText login_txtPassword;
	private Button login_btnLogin;
	private Button login_btnOlvidar;
	
	private String msg;
	private SharedPreferences prefs;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        setContentView(R.layout.activity_login);
        
        // Cargo la referencia al archivo de preferencias
        prefs = getSharedPreferences(PrefConsts._FILENAME, MODE_PRIVATE);
        
        bindComponents();
        assignEvents();
        setDefaults();
        inicializar();
    }

    private void setDefaults() {
		String usuario = prefs.getString(PrefConsts.USERNAME_FIELD, PrefConsts.USERNAME_DEFAULT);
		login_txtUsuario.setText(usuario);
	}

	@Override
    protected void onResume() {
    	super.onResume();
    	resetFields();
    	setDefaults();
    	inicializar();
    }
    
    private void inicializar() {
    	if (login_txtUsuario.getText().toString().equals("")) {
    		login_txtUsuario.requestFocus();
    	} else {
    		login_txtPassword.requestFocus();
    	}		
	}

	private void resetFields() {
		login_txtUsuario.setText("");
		login_txtPassword.setText("");
	}

	private void assignEvents() {
    	login_btnLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String sUsr, sPass;
				sUsr = login_txtUsuario.getText().toString();
				sPass = login_txtPassword.getText().toString();
				Log.i(TAG, "Login: " + sUsr + " " + sPass);
				
				if (validForm()) {
					Editor editor = prefs.edit();
					editor.putString(PrefConsts.USERNAME_FIELD, sUsr);
					editor.commit();
					Intent intent = new Intent(MainActivity.this, HelloActivity.class);
					intent.putExtra(IntentKeys.LOGIN_USERNAME, sUsr);
					startActivity(intent);
				} else {
					Toast toastMsg = Toast.makeText(
							getApplicationContext(), msg, Toast.LENGTH_SHORT);
					toastMsg.show();
				}
			}
		});
    	login_btnOlvidar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Editor editor = prefs.edit();
				editor.remove(PrefConsts.USERNAME_FIELD);
				editor.commit();
				resetFields();
				Toast toastMsg = Toast.makeText(MainActivity.this, "Usuario olvidado", Toast.LENGTH_SHORT);
				toastMsg.show();
				login_txtUsuario.requestFocus();
			}
		});
	}

	protected boolean validForm() {
		boolean retorno = false;
		
		if (login_txtUsuario.getText().toString().length() == 0) {
			msg = "Ingrese un usuario!";
		} else if (login_txtPassword.getText().toString().length() == 0) {
			msg = "Ingrese una clave!";
		} else if (!login_txtPassword.getText().toString().equals(PASSWORD)) {
			msg = "Clave incorrecta!";
		} else {
			retorno = true;
		}
		
		return retorno;
	}


	private void bindComponents() {
		login_txtUsuario = (EditText)findViewById(R.id.login_txtUsuario);
		login_txtPassword = (EditText)findViewById(R.id.login_txtPassword);
		login_btnLogin = (Button)findViewById(R.id.login_btnLogin);
		login_btnOlvidar = (Button)findViewById(R.id.login_btnOlvidar);
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
        	Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        	startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
