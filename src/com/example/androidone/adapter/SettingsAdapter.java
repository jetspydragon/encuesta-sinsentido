package com.example.androidone.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.androidone.R;
import com.example.androidone.objects.MenuItem;

public class SettingsAdapter extends BaseAdapter {

	private ArrayList<MenuItem> menuItems;
	private Context context;
	
	public SettingsAdapter(ArrayList<MenuItem> menuItems, Context context) {
		this.menuItems = menuItems;
		this.context = context;
	}
	
	@Override
	public int getCount() {
		return menuItems.size();
	}

	@Override
	public MenuItem getItem(int position) {
		return menuItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	class ViewHolder {
		TextView lblItem;
		CheckBox chkValue;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MenuItem menuItem = menuItems.get(position);
		ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.row_settings, parent, false);
			holder = new ViewHolder();
			holder.lblItem = (TextView)convertView.findViewById(R.id.r_settings_text);
			holder.chkValue = (CheckBox)convertView.findViewById(R.id.r_settings_value);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		holder.lblItem.setText(menuItem.getText());
		holder.chkValue.setChecked(menuItem.getValue());;
		
		return convertView;
	}

}
