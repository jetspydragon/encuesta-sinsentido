package com.example.androidone;

public class PrefConsts {
	public static final String _FILENAME = "CursoPrefs";
	public static final String USERNAME_FIELD = "usuario";
	public static final String USERNAME_DEFAULT = "";
	// Encuesta
	public static final String SEXODURO_FIELD = "SexoDuro";
	public static final String SEXONORMAL_FIELD = "SexoNormal";
	public static final String ESTADOCIVIL_FIELD = "EstadoCivil";
	public static final String EDAD_FIELD = "Edad";
	public static final String ESTADO_FIELD = "Estado";
	public static final String INTERES_FIELD = "Interes";
	public static final boolean SEXODURO_DEFAULT = false; 
	public static final boolean SEXONORMAL_DEFAULT = false;
	public static final int ESTADOCIVIL_DEFAULT = 0;
	public static final int EDAD_DEFAULT = 0;
	public static final int ESTADO_DEFAULT = 0;
	public static final float INTERES_DEFAULT = 0; 
}
