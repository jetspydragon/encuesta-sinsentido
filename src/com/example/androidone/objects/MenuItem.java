package com.example.androidone.objects;

public class MenuItem {
	
	public static int MENUTYPE_TITLE = 0;
	public static int MENUTYPE_ITEM = 1;
	
	private int menuType;
	private String text;
	private boolean value;
	
	/**
	 * @param menuType
	 * @param text
	 * @param value
	 */
	public MenuItem(int menuType, String text, boolean value) {
		this.menuType = menuType;
		this.text = text;
		this.value = value;
	}
	
	public MenuItem() {
		this.menuType = MENUTYPE_ITEM;
		this.text = "Default item";
		this.value = false;
	}
	
	/**
	 * @return the menuType
	 */
	public int getMenuType() {
		return menuType;
	}
	/**
	 * @param menuType the menuType to set
	 */
	public void setMenuType(int menuType) {
		this.menuType = menuType;
	}
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return the value
	 */
	public boolean getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(boolean value) {
		this.value = value;
	}
	
	
}
